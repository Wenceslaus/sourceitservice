package com.sourceit.sourceitservicebinding;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements MainService.Counter {

    MainService mainService;
    boolean mBound;
    public ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName name, IBinder binder) {
            Log.d("serviceConnection", "MainActivity onServiceConnected");
            mainService = ((MainService.LocaleBinder) binder).getService();
            mainService.setCountListener(MainActivity.this);
            mBound = true;
        }

        public void onServiceDisconnected(ComponentName name) {
            Log.d("serviceConnection", "MainActivity onServiceDisconnected");
            mBound = false;
        }
    };
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.getter);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, mainService.getRandom(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, MainService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            mainService.stopWork();
            mainService.setCountListener(null);
            unbindService(serviceConnection);
            mBound = false;
        }
    }

    @Override
    public void action() {
        Toast.makeText(MainActivity.this, "all done", Toast.LENGTH_SHORT).show();
    }
}
