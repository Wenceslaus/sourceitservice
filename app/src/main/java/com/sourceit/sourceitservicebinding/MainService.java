package com.sourceit.sourceitservicebinding;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Random;

/**
 * Created by wenceslaus on 29.10.16.
 */

public class MainService extends Service {

    LocaleBinder localeBinder = new LocaleBinder();
    Counter counter;
    Handler handler;
    Runnable task = new Runnable() {
        @Override
        public void run() {
            if (counter != null) {
                counter.action();
            }
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        handler = new Handler();
        handler.postDelayed(task, 4000);
        return localeBinder;
    }

    public void setCountListener(Counter counter) {
        this.counter = counter;
    }

    public String getRandom() {
        return new Random().nextInt() + "!";
    }

    public void stopWork() {
        handler.removeCallbacks(task);
    }

    public interface Counter {
        void action();
    }

    public class LocaleBinder extends Binder {

        MainService getService() {
            return MainService.this;
        }

    }
}
